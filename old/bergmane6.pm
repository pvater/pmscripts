use application "tropical";

# foreach ("E6", "E7") {
foreach ("E6") {
  print "$_ :\n";
  my $rs = polytope::root_system($_);
  my $mat = new Matrix<QuadraticExtension>($rs->VECTORS);
  $mat->col(0) = ones_vector($mat->rows);
  # my $matroid = new matroid::Matroid(VECTORS=>$mat);
  # my $bergman = matroid_fan_from_flats<Min>($matroid);
  print matroid_fan<Min>(transpose($mat))->F_VECTOR;
  # my $bergman = matroid_fan<Min>(transpose($mat));
  # print $bergman->F_VECTOR;
  # save($bergman, "$_.poly");
  print "\n";
}
