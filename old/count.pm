use Benchmark qw(:all);
use application "polytope";
use Math::Trig;
use strict;
use warnings;
$Polymake::User::Verbose::cpp = 1;

# # TODO
# sub curvature {
# }

my $dim = 3;
my $deg = 3;
# my $n_pts = 5000;
# my $n_pts = [100,500,1000];

# my $pts = rand_sphere($dim, $n_pts)->POINTS->minor(All, ~[0]);

my $support = polytope::simplex($dim, $deg)->LATTICE_POINTS->minor(All, ~[0]);
# my $orthants = cube($dim,1,0)->VERTICES->minor(All, ~[0]);

# TODO different sign distributions
# my @signs = (0) x $support->rows;
# my @signs = (1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0);
# my @signs = (1,0,1,0,1,0,1,0,1,0,0,1,0,1,0,1,1,0,1,0);
# my @signs = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
# my @signs = (0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,0,0,0,1);

### iterate triangulations:
my $filename = "$ARGV[0]";
open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";
while (my $str = <$fh>) {

  ### parse line to array of arrays:
  chomp $str;
  my @ar = ( $str =~ /\{[\d\,]+\}/g );
  my @triang;
  foreach (@ar) {
    my @foo = ( $_ =~ /\d+/g );
    push @triang, \@foo;
  }

  my $np_verts_set = new Set();
  my @triang_prim = ();
  for my $simplex (@triang) {
    my @simplex_tail = @$simplex[1..$#{$simplex}];
    my $nvol = abs(det($support->minor(\@simplex_tail,All) - repeat_row($support->row(@$simplex[0]),3)));
    if ($nvol != 1) {
      push @triang_prim, $simplex;
      $np_verts_set += new Set($simplex);
    }
  }
  print $np_verts_set->size, "\n";
  # print $#triang_prim+1, "\n";

}
