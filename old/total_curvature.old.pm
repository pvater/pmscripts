use Benchmark qw(:all);
use application "polytope";
use strict;
use warnings;
$Polymake::User::Verbose::cpp = 1;

# TODO
# sub curvature {
# }

my $dim = 3;
my $deg = 3;
my $n_pts = 100;
# my $pts = rand_sphere($dim, $n_pts)->POINTS->minor(All, ~[0]);

my $support = polytope::simplex($dim, $deg)->LATTICE_POINTS->minor(All, ~[0]);
my $orthants = cube($dim,1,0)->VERTICES->minor(All, ~[0]);

# TODO different sign distributions
my @signs = (0) x $support->rows;

# iterate triangulations:

# TODO

# my $triang = [[0,1,4,10]];
my $triang = [[0,1,4,10],[9,10,11,13],[10,11,13,16],[15,16,17,18],[16,17,18,19],[2,3,6,12],[2,6,8,12],[11,12,14,17],[11,14,15,17],[8,11,12,14],[9,11,13,15],[11,13,15,16],[1,4,7,10],[1,2,5,11],[5,8,9,11],[5,7,9,11],[2,5,8,11],[2,8,11,12],[11,15,16,17],[1,7,10,11],[1,5,7,11],[8,11,14,15],[8,9,11,15],[7,9,10,11]];

# per triangulation:

# // sign distribution for this orthant:
# Vector<bool> S(monomials.rows());
# for (int m = 0; m < monomials.rows(); m++)
#    S[m] = ((0|orthant)*monomials[m] % 2 != signs[m]);

# my $t0= Benchmark->new;
#   $r->FACETS;
# my $t1=Benchmark->new;
# my $td1=timediff($t1,$t0);
# print "FACETS: ".timestr($td1)."\n";

my $T0 = Benchmark->new;
my $curv_total = new Float(0);
for my $orthant (@{rows($orthants)}) {
  my $t0 = Benchmark->new;
  print "orthant: ", $orthant, "\n";
  my @signs_in_o = @signs;
  foreach (0 .. $#signs_in_o-1) {
    $signs_in_o[$_] = ((new Vector<Integer>($orthant)) * $support->row($_) % 2 != $signs[$_]);
  }
  for my $simplex (@$triang) {
    my @simplex_positive = grep { $signs_in_o[$_] } @$simplex;
    my @simplex_negative = grep { !$signs_in_o[$_] } @$simplex;
    my @rays = ();
    for my $p (@simplex_positive) {
      for my $n (@simplex_negative) {
        push @rays, ($support->row($p)-$support->row($n));
      }
    }
    my $cone = new Cone(RAYS=>\@rays);

    ### compute curvature:
    my $pts = rand_sphere($dim, $n_pts)->POINTS->minor(All, ~[0]);
    my $count = new Rational(0);
    if ($cone->DIM > 0) {
      # foreach (@{rows($pts)}) {
      foreach (@$pts) {
        $count++ if $cone->contains($_);
      }
    }
    $curv_total += $count;
    # print "$_ " for @$simplex;
    # print "\n";
  }
  my $t1 = Benchmark->new;
  my $td = timediff($t1,$t0);
  print "time: ".timestr($td)."\n";
}
print "curv: ", new Float($curv_total/$n_pts), "\n";
my $T1 = Benchmark->new;
my $TD = timediff($T1,$T0);
print "time total: ".timestr($TD)."\n";
