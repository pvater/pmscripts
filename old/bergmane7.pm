use application "tropical";

# foreach ("E6", "E7") {
foreach ("E7") {
  print "$_ :\n";
  my $rs = polytope::root_system($_);
  my $mat = new Matrix<QuadraticExtension>($rs->VECTORS);
  $mat->col(0) = ones_vector($mat->rows);
  my $bergman = matroid_fan<Min>(transpose($mat));
  print $bergman->F_VECTOR;
  save($bergman, "$_.poly");
  print "\n";
}
