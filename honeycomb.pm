use application "tropical";
# use Benchmark qw(:all :hireswallclock);
# use Math::Trig;
# use strict;
# use warnings;
# $Polymake::User::Verbose::cpp = 1;

my $n = 5;
my @monoms;
my @weights;
my @points;
# my @signs;
foreach my $i (0 .. $n) {
  foreach my $j (0 .. $n-$i) {
    foreach my $k (0 .. $n-$i-$j) {
      push @weights, (($i+$j+$k)*($i+$j+$k) - $i*$j/2.0 - $i*$k/3.0 - $j*$k/5.0);
      push @points, [1, $i, $j, $k];
      push @monoms, [$n-$i-$j-$k, $i, $j, $k];
      # push @signs, ($i*$j+$i+$j)%2;
    }
  }
}
# my $h = new Hypersurface<Min>(MONOMIALS=>\@monoms,COEFFICIENTS=>\@weights);
# $h->VISUAL;

my $s = new fan::SubdivisionOfPoints(POINTS=>\@points,WEIGHTS=>\@weights);
$s->VISUAL;
# print $s->VECTORS;
# print $s->MAXIMAL_CELLS;
# print $s->MIN_WEIGHTS;

# vim: set ft=perl:
