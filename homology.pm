# arguments:
#   1. degree
#   2. nr of random signdist's
#   3. nr of fixed leading bits
#   4. fixed leading bits
#
# invocation:
#   env GLIBCXX_FORCE_NEW=1 <script> <args>
#   (ensures freeing of memory after each loop)

use application "tropical";
use Config;
# my $randbits = $Config{randbits};
use constant RANDBITS => $Config{randbits};
use constant RAND_MAX => 2**RANDBITS;

use strict;
use warnings;
# use Benchmark qw(:all :hireswallclock);
# $Polymake::User::Verbose::cpp = 1;

my $d = "$ARGV[0]" || 3;
my $n = "$ARGV[1]" || 1000;
my $n_fixed = "$ARGV[2]" || 1;
my $sign_fixed = "$ARGV[3]" || 0;
# my $filename = "$ARGV[4]" . ".data" || "foo.data";

# my $d = 5;
my @monoms;
my @weights;
my @points;
# my @signs;
foreach my $i (0 .. $d) {
  foreach my $j (0 .. $d-$i) {
    foreach my $k (0 .. $d-$i-$j) {
      push @weights, (($i+$j+$k)*($i+$j+$k) - $i*$j/2.0 - $i*$k/3.0 - $j*$k/5.0);
      push @points, [1, $i, $j, $k];
      push @monoms, [$d-$i-$j-$k, $i, $j, $k];
      # push @signs, ($i*$j*k+$i+$j+$k)%2;
    }
  }
}
my $h = new Hypersurface<Min>(MONOMIALS=>\@monoms,COEFFICIENTS=>\@weights);
# $h->VISUAL;

# my $n = 10000;
# my $seen = new Set;

my $n_monoms = $h->MONOMIALS->rows;
my $n_free = $n_monoms - $n_fixed;

print $n_monoms, ", ", $n_free, "\n";

# my %bettis;
my $bettis = new Map<Array<Int>,String>;
my $nr_bettis = new Map<Array<Int>,Int>;
for my $nn (1 .. $n) {

  # my $ss;
  # if ($n_free > 47) {
  #   my $ss1 = int(rand(1<<($n_free-47)));
  #   my $ss2 = int(rand(1<<47));
  #   $ss = ($ss1<<47) | $ss2;
  # } else {
  #   $ss = int(rand(1<<$n_free));
  # }
  # # $seen += $ss;

  # my $str = (sprintf "%.${n_fixed}b", $sign_fixed) . (sprintf "%.${n_free}b", $ss);
  # my @sa = split //, $str;
  # print @sa, "\n";

  my $str = (sprintf "%.${n_fixed}b", $sign_fixed);
  my $tmp = $n_free;
  # for my $i (1 .. (ceil($n_free/RANDBITS))) {
  while ($tmp > 0) {
    # print $i,"\n";
    # my $ss = int(rand(1<<$tmp));
    my $min = min($tmp,RANDBITS);
    $str = $str . (sprintf "%.${min}b", int(rand(1<<$min)));
    $tmp -= RANDBITS;
  }
  # print $str, "\n";
  my @sa = split //, $str;
  print @sa, "\n";

  my $hp = $h->PATCHWORK(SIGNS=>\@sa);
  my $betti = $hp->betti_numbers_z2;
  $bettis->{$betti} = $str;
  $nr_bettis->{$betti} += 1;

  # $bettis += scalar2set($h->PATCHWORK(SIGNS=>\@sa)->betti_numbers_z2);

  # $h->remove("PATCHWORK.REAL_PHASE");
  # $h->remove("PATCHWORK");

  # undef $str;
  # undef @sa;
  # undef $hp;
  # undef $betti;

}
# $h->properties();
print $bettis, "\n";
print $nr_bettis, "\n";

# vim: set ft=perl:
