#!/bin/perl

# count curvatures from results file

use strict;
use warnings;

my %c;
while (my $str = <STDIN>) {
  my $l = eval $1 if ($str =~ /(\[.*\])/);
  my $s = 2**(10 - scalar @$l);
  my @ar = ( $str =~ / (\d+)/g );
  $c{$_}+=$s for @ar;
  # print $_,"," for @$l;
}
print "$_ $c{$_}\n" for (keys %c);
print "========\n";
print "$_ ",$c{$_}/8,"\n" for (keys %c);
