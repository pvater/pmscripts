# compute curvatures of all triangulations

# use Benchmark qw(:all :hireswallclock);
use application "polytope";
use Math::Trig;
use strict;
use warnings;
# $Polymake::User::Verbose::cpp = 1;

# TODO make this work for arbitrary dim/deg

my $dim = 3;
my $deg = 3;
# my $nr_simplices = $deg**$dim;
my $support = polytope::simplex($dim, $deg)->LATTICE_POINTS->minor(All, ~[0]);
my $orthants = cube($dim,1,0)->VERTICES->minor(All, ~[0]);
# my $max_nr_ne_verts = 10;
my $global_sign_dists = new Matrix<Int>(zero_vector()|cube(9,1,0)->VERTICES->minor(All, ~[0]));

# my $curv_triv = 54*pi;
# my $curv_triv = $nr_simplices*2*pi;

# my $ne_simplices = load_data("objects/non_elementary.poly");
my $all_ne_curvatures = load_data("objects/ne_curvatures.poly");
my @all_ne_array = keys %$all_ne_curvatures;
my $all_ne_set = new Set<Array<Int>>(\@all_ne_array);

### iterate triangulations:
my $filename = "$ARGV[0]";
open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";
while (my $str = <$fh>) {

  ### parse line to array of array(ref)s:
  chomp $str;
  my @ar = ( $str =~ /\{[\d\,]+\}/g );
  my @triang;
  foreach (@ar) {
    my @foo = ( $_ =~ /\d+/g );
    push @triang, \@foo;
  }

  ### skip bad input:
  next if (scalar @ar == 0);

  ### print unique identifier of triangulation:
  # $str =~ /^(\(.+\))/ && print "$1";
  # $str =~ /(\([\d\.]+\))/ && print "$1 ";

  ### find non-elementary simplices:
  my @this_ne_array = ();
  my @this_ne_vec = ();
  my @this_ne_index = ();
  my $ne_verts_set = new Set();
  if ($#triang+1 != 27) { # catch early if unimodular
    my $foo = 0;
    for my $simplex (@triang) {
      my $simplex_vec = new Array<Int>($simplex);
      if ($all_ne_set->contains($simplex_vec)) {
        push @this_ne_index, $foo;
        push @this_ne_array, $simplex;
        push @this_ne_vec, $simplex_vec;
        $ne_verts_set += new Set($simplex);
      }
      $foo++;
    }
  }
  my @ne_verts = @$ne_verts_set;

  # ### catch if no non-elementary simplices:
  # if ($ne_verts_set->size == 0) {
  #   print "[] ", ()*2*Math::Trig::pi, "\n";
  #   next;
  # }

  my @this_ne_verts_index = ();
  for (my $i = 0; $i < @ne_verts; $i++) {
    $this_ne_verts_index[$ne_verts[$i]] = $i;
  }

  ### print things:
  $str =~ /^(\(.+\))/ && print "$1";
  print " [", join(",",@this_ne_index), "]";
  print " [", join(",",@ne_verts), "]";

  if (scalar @ne_verts == 0) {
    print " ", (scalar @triang)*2*pi;
  } else {
    # for my $sign_dist (@{rows($global_sign_dists->minor(range(0,(1<<($#ne_verts))-1),All))}) {
    for my $sign_dist (@{rows($global_sign_dists->minor(range(0,(1<<($#ne_verts))-1),All))}) {
      my $curv_total = new Float(0);
      my $i = 0;
      for my $simplex (@this_ne_array) {
        my $sign_num = $sign_dist->[$this_ne_verts_index[$simplex->[1]]] + ($sign_dist->[$this_ne_verts_index[$simplex->[2]]])*2 + ($sign_dist->[$this_ne_verts_index[$simplex->[3]]])*4;
        $sign_num = 7 - $sign_num if ($sign_dist->[$this_ne_verts_index[$simplex->[0]]]);
        # $curv_total += $M->{$this_ne_vec[$i]}->[$sign_num];
        $curv_total += $all_ne_curvatures->{$this_ne_vec[$i]}->[$sign_num];
        $i++;
      }
      $curv_total += ($#triang - $#this_ne_array) * 2 * pi;
      print " ", $curv_total;
    }
  }

  print "\n";

} # loop triangulations
