# gather all non-elementary (and non-primitive) simplices from the triangulation file

use Benchmark qw(:all);
use application "polytope";
# use Math::Trig;
use strict;
use warnings;
$Polymake::User::Verbose::cpp = 1;

my $dim = 3;
my $deg = 3;

my $support = polytope::simplex($dim, $deg)->LATTICE_POINTS->minor(All, ~[0]);
# my $orthants = cube($dim,1,0)->VERTICES->minor(All, ~[0]);

# my @not_unimodular = ();
# my @not_elementary = ();

my $nu_set = new Set<Set<Int>>();
my $ne_set = new Set<Set<Int>>();

### iterate triangulations:
my $filename = "$ARGV[0]";
open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";
while (my $str = <$fh>) {

  ### parse line to array of arrays:
  chomp $str;
  my @ar = ( $str =~ /\{[\d\,]+\}/g );
  my @triang;
  foreach (@ar) {
    my @foo = ( $_ =~ /\d+/g );
    push @triang, \@foo;
  }

  my @not_unimodular = ();
  my @not_elementary = ();
  for my $simplex (@triang) {
    my @simplex_tail = @$simplex[1..$#{$simplex}];
    my $sdet = det($support->minor(\@simplex_tail,All) - repeat_row($support->row(@$simplex[0]),3));
    if (abs($sdet) != 1) { # not unimodular
      push @not_unimodular, $simplex;
      my $simplex_set = new Set($simplex);
      # $nu_set += scalar2set(new Set<Int>($simplex));
      $nu_set->collect($simplex_set);
      if ($sdet%2 != 1) { # not elementary
        push @not_elementary, $simplex;
        # $ne_set += scalar2set(new Set<Int>($simplex));
        $ne_set->collect($simplex_set);
      }
    }
  }

  # print "nu: ";
  # foreach my $s (@not_unimodular) {
  #   print "(";
  #   print $_,"," for @$s;
  #   print "),";
  # }
  # print "\n";
  # print "ne: ";
  # foreach my $s (@not_elementary) {
  #   print "(";
  #   print $_,"," for @$s;
  #   print "),";
  # }
  # print "\n";

}

print $nu_set,"\n";
print $ne_set,"\n";

save_data($nu_set,"non_unimodular.poly");
save_data($ne_set,"non_elementary.poly");
