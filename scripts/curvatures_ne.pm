# TODO
# compute curvatures of non-elementary simplices

# use Benchmark qw(:all :hireswallclock);
use application "polytope";
use Math::Trig;
use strict;
use warnings;
$Polymake::User::Verbose::cpp = 1;

my $dim = 3;
my $deg = 3;
my $support = polytope::simplex($dim, $deg)->LATTICE_POINTS->minor(All, ~[0]);
my $orthants = cube($dim,1,0)->VERTICES->minor(All, ~[0]);
my $global_sign_dists = new Matrix<Int>(zero_vector()|cube(9,1,0)->VERTICES->minor(All, ~[0]));

my $curv_triv = 54*pi;

my $M = new HashMap<Vector<Int>,Array<Float>>; # remembers non-trivial curvatures

### iterate triangulations:
my $filename = "$ARGV[0]";
open(my $fh, '<:encoding(UTF-8)', $filename)
  or die "Could not open file '$filename' $!";
while (my $str = <$fh>) {

  ### parse line to array of array(ref)s:
  chomp $str;
  my @ar = ( $str =~ /\{[\d\,]+\}/g );
  my @triang;
  foreach (@ar) {
    my @foo = ( $_ =~ /\d+/g );
    push @triang, \@foo;
  }

  if (scalar @ar == 0) {
    next;
  }

  $str =~ /(\([\d\.]+\))/ && print "$1 ";

  # my $T0 = Benchmark->new;

  ### catch if 27 simplices:
  if ($#triang+1 == 27) {
    print "[] ", $curv_triv, "\n";
    next;
  }

  ### find non-primitive simplices:
  my @non_primitive = ();
  my @non_primitive_vec = ();
  my $np_verts_set = new Set();
  for my $simplex (@triang) {
    my @simplex_tail = @$simplex[1..$#{$simplex}];
    # TODO this mod 2 for 'elementary' instead of 'primitive':
    # my $nvol = (det($support->minor(\@simplex_tail,All) - repeat_row($support->row(@$simplex[0]),3)))%2;
    my $nvol = abs(det($support->minor(\@simplex_tail,All) - repeat_row($support->row(@$simplex[0]),3)));
    if ($nvol != 1) {
      push @non_primitive, $simplex;
      push @non_primitive_vec, new Vector<Int>($simplex);
      $np_verts_set += new Set($simplex);
    }
  }

  # my $T1 = Benchmark->new;

  ### find all curvatures in non-primitive simplices:
  for my $simplex (@non_primitive) {
    my $simplex_vec = new Vector<Int>($simplex);
    if (!defined $M->{$simplex_vec}) {
      print STDERR "-> new entry: ", $simplex_vec, "\n";
      $M->{$simplex_vec} = new Array<Float>(8);
      # $M->{$simplex_vec} = new Array<Float>(16);
      my $signs_num = 0;
      for my $signs (@{rows(zero_vector()|$orthants)}) {
        my $curv_total = 0;
        for my $orthant (@{rows($orthants)}) {
          my $orthant_vec = new Vector<Int>($orthant);

          # print "signs: ", $signs, " orthant: ", $orthant;

          ### compute sign distribution in this orthant:
          my @signs_in_o = ();
          foreach (0 .. $signs->dim-1) {
            $signs_in_o[$_] = ($orthant_vec * $support->row(@$simplex[$_]) % 2 != $signs->[$_]);
          }

          ### compute curvature cone:
          my $count = 0;
          my @simplex_positive = grep { $signs_in_o[$count++] } @$simplex;
          $count = 0;
          my @simplex_negative = grep { !$signs_in_o[$count++] } @$simplex;
          my @rays = ();
          for my $p (@simplex_positive) {
            for my $n (@simplex_negative) {
              push @rays, ($support->row($p)-$support->row($n));
            }
          }

          ### compute curvature:
          if ($#rays >= 0) {
            my $cone = new Cone(RAYS=>\@rays,LINEAR_SPAN=>[]);
            my $facets_normalized = normalized(new Matrix<Float>($cone->FACETS));
            foreach (@{$cone->FACETS_THRU_RAYS}) {
              my $n = new Matrix<Float>($facets_normalized->minor($_, All));
              my $ang = acos_real(-($n->row(0))*($n->row(1)));
              $curv_total += $ang;
            }
            $curv_total -= ($#rays - 1) * pi;
          }

        }
        $M->{$simplex_vec}->[$signs_num] = $curv_total;
        $signs_num++;
      }
    }
  }

  # my $T2 = Benchmark->new;

  my @np_verts = @$np_verts_set;
  print "[",join(",",@np_verts),"]";
  my @np_index = ();
  for (my $i = 0; $i < @np_verts; $i++) {
    $np_index[$np_verts[$i]] = $i;
  }

  ### find curvatures of triangulation:
  for my $sign_dist (@{rows($global_sign_dists->minor(range(0,(1<<($#np_verts))-1),All))}) {
    my $curv_total = new Float(0);
    my $i = 0;
    for my $simplex (@non_primitive) {
      # my $signs = $sign_dist->slice($simplex);
      # my $sign_num = ($signs[0] != $signs[1]) + ($signs[0] != $signs[2])*2 + ($signs[0] != $signs[3])*4;
      # print $sign_dist->type->full_name;
      # my $foo = $simplex->[0];
      # my $goo = $np_index[$foo];
      # my $s_0 = $sign_dist->[$goo];
      # my $sign_num = $sign_dist->[$np_index[$simplex->[1]]] + ($sign_dist->[$np_index[$simplex->[2]]])<<1 + ($sign_dist->[$np_index[$simplex->[3]]])<<2;
      my $sign_num = $sign_dist->[$np_index[$simplex->[1]]] + ($sign_dist->[$np_index[$simplex->[2]]])*2 + ($sign_dist->[$np_index[$simplex->[3]]])*4;
      # print $sign_num, " ";
      $sign_num = 7 - $sign_num if ($sign_dist->[$np_index[$simplex->[0]]]);
      # print $sign_num, " ";
      # my $s_0 = $sign_dist->[$np_index[$simplex->[0]]];
      # my $sign_num = ($s_0 != $sign_dist->[$np_index[$simplex->[1]]]) + ($s_0 != $sign_dist->[$np_index[$simplex->[2]]])*2 + ($s_0 != $sign_dist->[$np_index[$simplex->[3]]])*4;
      $curv_total += $M->{$non_primitive_vec[$i]}->[$sign_num];
      # $curv_total += $M->{new Vector<Int>($simplex)}->[$sign_num];
      $i++;
    }
    $curv_total += ($#triang - $#non_primitive) * 2 * pi;
    print " ", $curv_total;
  }
  print "\n";

  # my $T3 = Benchmark->new;
  # my $TD1 = timediff($T1,$T0);
  # my $TD2 = timediff($T3,$T2);
  # print timestr($TD1), "\n";
  # print timestr($TD2), "\n";

} # loop triangulations
# print "\n", $M, "\n";
save_data($M, "npdict.poly");
