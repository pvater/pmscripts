$points3 = polytope::simplex(3,3)->LATTICE_POINTS;
$weights3 = [5, 1, 1, 5, 2, 0, 2, 2, 2, 5, 2, 0, 2, 0, 0, 1, 2, 2, 1, 5];
$sub3 = new fan::SubdivisionOfPoints(POINTS=>$points3,WEIGHTS=>$weights3);
$cells3 = $sub3->MAXIMAL_CELLS;

$set3 = new Set<Matrix>;
for my $cell (0 .. 26) {
  my $matrix = new Matrix;
  for my $elem (@{$cells3->[$cell]}) {
    $matrix /= $points3->[$elem];
  }
  $set3 += $matrix;
}

$zm = zero_matrix(4,1);
$om = ones_matrix(4,1);

$set5 = new Set<Matrix>;
for my $mat (@{$set3}) {
  $set5 += $mat;
  $set5 += ($mat + ($zm|$om|$zm|$zm));
  $set5 += ($mat + ($zm|$zm|$om|$zm));
  $set5 += ($mat + ($zm|$zm|$zm|$om));
  $set5 += ($mat + ($zm|$om|$om|$zm));
  $set5 += ($mat + ($zm|$om|$zm|$om));
  $set5 += ($mat + ($zm|$zm|$om|$om));
  $set5 += ($mat + 2*($zm|$om|$zm|$zm));
  $set5 += ($mat + 2*($zm|$zm|$om|$zm));
  $set5 += ($mat + 2*($zm|$zm|$zm|$om));
}

$points5 = polytope::simplex(3,5)->LATTICE_POINTS;
$pointsmap = new Map<Vector,Int>;
foreach (0 .. $points5->rows-1) {
  $pointsmap->{$points5->[$_]} = $_;
}
$cells5 = new IncidenceMatrix(0,$points5->rows);
for my $mat (@{$set5}) {
  my $incidence = new Set;
  for my $point (@{rows($mat)}) {
    $incidence += $pointsmap->{$point};
  }
  $cells5 /= $incidence;
}
$sub5 = new fan::SubdivisionOfPoints(POINTS=>$points5,MAXIMAL_CELLS=>$cells5);
